const container = document.querySelector('.boxes')
const boxes = new Array(16).fill().map((_,i) => i)

const createBox = n => {
  const el = document.createElement('div')
  el.classList.add('box')
  el.dataset.item = n
  return el 
}

const createButton = () => {
  const el = document.createElement('button')
  el.classList.add('add')
  el.innerText = 'Add A Box'

  el.addEventListener('click', () => {
    boxes.push(boxes.length) 
    render(boxes)
  })

  return el
}

const render = (boxes) => {
  const frag = document.createDocumentFragment() 
  boxes.map(createBox).forEach(el => frag.appendChild(el))
  frag.appendChild(createButton())
  container.innerHTML = ''
  container.appendChild(frag)
}

render(boxes)
