module.exports = [
  {name: 'Bob', age: 34, occupation: 'Business Management', friends: []},
  {name: 'Sally', age: 40, occupation: 'Software Architect', friends: ['Joe', 'Frank']},
  {name: 'Joe', age: 23, occupation: 'Marketing', friends: ['Sally']},
];
